#! /bin/bash 

# Setup monitors
xrandr --output DisplayPort-0 --primary --output HDMI-A-0 --auto &

# Transparency and shadows
picom &

# Wallpaper manager
conky -d -c ~/.cache/wal/colors-conky.conf &
wal -R &

# Start xscreensaver
xscreensaver -no-splash &

# Restore openrgb colors
~/bin/manjaro/set_open_rgb_colors.sh &

# Start squeezlite
squeezelite -n "Desktop computer $(hostname)" &

# Launch applications
thunderbird &
joplin-desktop &
rambox &
logseq &
