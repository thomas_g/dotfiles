# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
import socket
import subprocess
import json
from libqtile.config import Key, Screen, Group, ScratchPad, DropDown, Drag, Click, Match
from libqtile.lazy import lazy
from libqtile import layout, bar, widget, hook
from datetime import datetime
from typing import List  # noqa: F401

mod = "mod4"
home = os.path.expanduser('~')
myTerm = "alacritty"

##### KEYBINDINGS #####
keys = [
  # Switch focus
  Key([mod], "h", lazy.layout.left()),
  Key([mod], "j", lazy.layout.down()),
  Key([mod], "k", lazy.layout.up()),
  Key([mod], "l", lazy.layout.right()),
  Key([mod], "space", lazy.layout.next()),  # Switch window focus to other pane(s) of stack
  Key([mod], "period", lazy.next_screen()), # Move monitor focus to next screen
  Key([mod], "comma", lazy.prev_screen()),  # Move monitor focus to prev screen

  # Move windows
  Key([mod, "shift"], "h", lazy.layout.shuffle_left()),
  Key([mod, "shift"], "j", lazy.layout.shuffle_down()),
  Key([mod, "shift"], "k", lazy.layout.shuffle_up()),
  Key([mod, "shift"], "l", lazy.layout.shuffle_right()),

  # Flip the layout
  Key([mod, "mod1"], "h", lazy.layout.flip_left()),  # BSP
  Key([mod, "mod1"], "j", lazy.layout.flip_down()),  # BSP
  Key([mod, "mod1"], "k", lazy.layout.flip_up()),    # BSP
  Key([mod, "mod1"], "l", lazy.layout.flip_right()), # BSP
  Key([mod, "mod1"], "space", lazy.layout.flip()),   # XmonadTall

  # Window controls
  Key([mod, "control"], "h", lazy.layout.grow_left()),     # BSP
  Key([mod, "control"], "j", lazy.layout.grow_down()),     # BSP
  Key([mod, "control"], "k", lazy.layout.grow_up()),       # BSP
  Key([mod, "control"], "l", lazy.layout.grow_right()),    # BSP
  Key([mod], "n", lazy.layout.normalize()),                # Restore all windows to default size ratios
  Key([mod], "m", lazy.layout.maximize()),                 # Toggle a window between minimum and maximum sizes
  Key([mod], "f", lazy.window.toggle_fullscreen()),        # Toggle fullscreen
  Key([mod, "shift"], "f", lazy.window.toggle_floating()), # Toggle floating

  # Keys for special events
  Key([mod], "Escape", lazy.spawn("xscreensaver-command -lock")),
  Key([], "XF86AudioRaiseVolume", lazy.spawn("amixer -q sset Master 2%+")),
  Key([mod, "control"], "equal", lazy.spawn("amixer -q sset Master 2%+")),
  Key([], "XF86AudioLowerVolume", lazy.spawn("amixer -q sset Master 2%-")),
  Key([mod, "control"], "minus", lazy.spawn("amixer -q sset Master 2%-")),
  Key([], "XF86AudioMute", lazy.spawn("amixer -q sset Master toggle")),
  Key([mod], "XF86AudioPlay", lazy.spawn(home + "/bin/manjaro/switch_audio_device.sh")),
  Key([mod, "control"], "0", lazy.spawn(home + "/bin/manjaro/switch_audio_device.sh")),
  Key([], "Print", lazy.spawn("import " + home + '/screenshots/' + datetime.now().strftime("%Y_%m_%d-%H_%M_%S") + '.png')),

  Key([mod], "Return", lazy.spawn(myTerm)),

  # Toggle between different layouts as defined below
  Key([mod], "Tab", lazy.next_layout()),
  Key([mod], "w", lazy.window.kill()),

  Key([mod, "control"], "r", lazy.restart()),
  Key([mod, "control"], "q", lazy.shutdown()),
  Key([mod], "r", lazy.spawncmd()),
  # Key([mod], "d", lazy.spawn("zsh -c '. /opt/asdf-vm/asdf.sh && rofi  -show combi -combi-modes window,drun,run'")),
  Key([mod], "d", lazy.spawn("rofi -show combi -combi-modes drun,run")),
  Key([mod], 'minus', lazy.group['scratchpad'].dropdown_toggle('keepass')),
  Key([mod], 'equal', lazy.group['scratchpad'].dropdown_toggle('gtop')),
]

##### COLORS #####
#Pywal Colors
colors = os.path.expanduser('~/.cache/wal/colors.json')
colordict = json.load(open(colors))

background = colordict['colors']['color0']
color_primary = colordict['colors']['color1']
color_primary_alt = colordict['colors']['color9']
color_secondary = colordict['colors']['color6']
color_secondary_alt = colordict['colors']['color14']
color_valid = colordict['colors']['color3']
color_alert = colordict['colors']['color4']

##### GROUPS #####
groups = [
  Group("1", layout = "bsp"),
  Group("2", layout = "bsp"),
  Group("3", layout = "bsp"),
  Group("4", layout = "bsp"),
  Group("5", layout = "bsp"),
  Group("6", layout = "bsp"),
  Group("7", layout = "bsp"),
  Group("8", layout = "bsp"),
  Group("9", layout = "bsp"),
  Group("0", layout = "bsp")
]

for group in groups:
  keys.append(Key([mod], group.name, lazy.group[group.name].toscreen()))        # Switch to another group
  keys.append(Key([mod, "shift"], group.name, lazy.window.togroup(group.name))) # Send current window to another group

# Scratchpad
groups.append(
  ScratchPad("scratchpad", [
    DropDown("keepass", "keepassxc", opacity = 1, x = 0.33, y = 0.2, height = 0.5, width = 0.33),
    DropDown("gtop", "alacritty -e gtop", opacity = 0.9, x = 0.5, y = 0, height = 1, width = 0.5),
  ])
)

##### DEFAULT THEME SETTINGS FOR LAYOUTS #####
layout_theme = {
  "border_width": 2,
  "margin": 3,
  "border_focus": color_secondary,
  "border_normal": color_primary
}

layout_large_margin_theme = {
  "border_width": 2,
  "margin": 20,
  "border_focus": color_secondary,
  "border_normal": color_primary
}

##### THE LAYOUTS #####
layouts = [
  layout.Bsp(fair = False, **layout_large_margin_theme),
  layout.Bsp(fair = False, **layout_theme),
  layout.Max(**layout_large_margin_theme),
  layout.Floating(**layout_theme),
  layout.RatioTile(**layout_large_margin_theme),
  layout.RatioTile(**layout_theme),
]

##### DEFAULT WIDGET SETTINGS #####
widget_defaults = dict(
  font = "MyFont bold",
  fontsize = 12,
  padding = 0,
  background = background,
  foreground = color_primary_alt
)
extension_defaults = widget_defaults.copy()

widget_icon = { 'padding': 8, 'fontsize': 20, 'font': 'MyFont' }
widget_separator = { 'linewidth': 0, 'padding': 40 }

##### WIDGETS #####
def widgets_default():
  return [
    widget.GroupBox(
      font="MyFont",
      fontsize = 12,
      margin_y = 1,
      margin_x = 0,
      padding_y = 5,
      padding_x = 5,
      borderwidth = 3,
      active = color_primary,
      inactive = color_primary_alt,
      rounded = False,
      highlight_color = background,
      highlight_method = "line",
      this_current_screen_border = color_secondary,
      this_screen_border = color_primary,
      other_current_screen_border = color_secondary_alt,
      other_screen_border = background
    ),
  ]

def widgets_window_name():
  return [
    widget.Spacer(),
    widget.WindowName(
      foreground = color_secondary
    )
  ]

def widgets_utils():
  return [
    widget.TextBox(text = "", **widget_icon),
    widget.CurrentLayout(),
    widget.Sep(**widget_separator),
    widget.TextBox(text = "", **widget_icon),
    widget.Volume(),
    widget.Sep(**widget_separator),
    widget.Clock(format = "%A, %B %d - %H:%M:%S"),
    widget.Sep(**widget_separator),
    widget.LaunchBar(
      progs = [('󰍃', 'qshell:self.qtile.shutdown()', 'logout from qtile')],
      text_only = True,
      **widget_icon
    ),
    widget.Sep(
      linewidth = 0,
      padding = 12
    ),
    widget.LaunchBar(
      progs = [('󰤄', 'systemctl suspend', 'puts computer to sleep')],
      text_only = True,
      **widget_icon
    ),
    widget.Sep(
      linewidth = 0,
      padding = 12
    ),
    widget.LaunchBar(
      progs = [('', 'shutdown -h now', 'power off computer')],
      text_only = True,
      **widget_icon
    ),
    widget.Sep(linewidth = 0, padding = 20),
  ]

def widgets_monitoring():
  return [
    widget.DF(
      warn_space = 300,
      format = '{uf}{m} free',
      warn_color = color_alert
    ),
    widget.Sep(**widget_separator),
    widget.Net(
      interface = "all",
      format = '{down:.0f}{down_suffix} ↓↑ {up:.0f}{up_suffix}'
    ),
    widget.Sep(**widget_separator),
    widget.TextBox(text = "", **widget_icon),
    widget.CPUGraph(
      graph_color = color_primary,
      fill_color = color_primary_alt,
      border_width = 0,
      line_width = 1,
      samples = 60,
      margin_x = 0,
      margin_y = 5
    ),
    widget.Sep(**widget_separator),
    widget.TextBox(text = "", **widget_icon),
    widget.Memory(
      measure_mem = 'G',
      format = '{MemUsed: .1f}{mm}'
    ),
    widget.Sep(**widget_separator),
    widget.ThermalSensor(threshold = 60, foreground_alert = color_alert, foreground = color_primary_alt),
    widget.Sep(**widget_separator),
    widget.TextBox(text = "󰏗", **widget_icon),
    widget.CheckUpdates(
      colour_have_updates = color_primary_alt,
      colour_no_updates = color_primary_alt,
      display_format = "{updates}",
      no_update_string = "-",
      execute = myTerm + " -e yay -Syu",
      distro = "Arch_yay",
      update_interval = 600
    ),
    widget.Sep(**widget_separator),
    widget.Systray(),
    widget.Sep(**widget_separator),
  ]

##### SCREENS #####
def init_screens():
  return [Screen(top=bar.Bar(widgets=widgets_default() + widgets_window_name() + widgets_monitoring() + widgets_utils(), size=30)),
          Screen(top=bar.Bar(widgets=widgets_default() + widgets_window_name() + widgets_utils(), size=30))]

if __name__ in ["config", "__main__"]:
  screens = init_screens()

##### DRAG FLOATING WINDOWS #####
mouse = [
  Drag([mod], "Button1", lazy.window.set_position_floating(),
       start=lazy.window.get_position()),
  Drag([mod], "Button3", lazy.window.set_size_floating(),
       start=lazy.window.get_size()),
  Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(
  border_focus = color_secondary,
  border_normal = color_primary,
  border_width = 2,
  float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    *layout.Floating.default_float_rules,
    Match(wm_class='confirmreset'), # gitk
    Match(wm_class='makebranch'), # gitk
    Match(wm_class='maketag'), # gitk
    Match(wm_class='ssh-askpass'), # ssh-askpass
    Match(title='branchdialog'), # gitk
    Match(title='pinentry'), # GPG key password entry
  ]
)
# floating_layout = layout.Floating(float_rules=[
#   {'wmclass': 'confirm'},
#   {'wmclass': 'dialog'},
#   {'wmclass': 'download'},
#   {'wmclass': 'error'},
#   {'wmclass': 'file_progress'},
#   {'wmclass': 'notification'},
#   {'wmclass': 'splash'},
#   {'wmclass': 'toolbar'},
#   {'wmclass': 'confirmreset'},  # gitk
#   {'wmclass': 'makebranch'},  # gitk
#   {'wmclass': 'maketag'},  # gitk
#   {'wname': 'branchdialog'},  # gitk
#   {'wname': 'pinentry'},  # GPG key password entry
#   {'wmclass': 'ssh-askpass'},  # ssh-askpass
# ])
auto_fullscreen = True
focus_on_window_activation = "smart"

##### STARTUP APPLICATIONS #####
@hook.subscribe.startup_once
def autostart():
  subprocess.call([home + '/.config/qtile/autostart.sh'])

@hook.subscribe.screen_change
def restart_on_randr(qtile, ev):
  qtile.cmd_restart()

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
