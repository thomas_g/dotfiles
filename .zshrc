# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"
# export PATH="$HOME/.yarn/bin:$HOME/.config/yarn/global/node_modules/.bin:$PATH"
# export PGHOST=localhost

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
ZSH_THEME="agnoster"
DEFAULT_USER="thomas"

# Which plugins would you like to load?
# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
  asdf
  autojump
  bundler
  git
  rails
  ruby
  zsh-autosuggestions
)
if [[ $(uname) == "Darwin" ]]; then
  plugins+=(brew)
else
  plugins+=(ssh-agent)
fi

source $ZSH/oh-my-zsh.sh
source $HOME/bin/functions/add_git_hook_after_clone.sh

# Source asdf
if [[ $(uname) == "Darwin" ]]; then
  source /usr/local/opt/asdf/Libexec/asdf.sh
else
. /opt/asdf-vm/asdf.sh
fi

# Wal config
if [[ $(uname) == "Linux" ]]; then
  # Import colorscheme from 'wal' asynchronously
  # &   # Run the process in the background.
  # ( ) # Hide shell job control messages.
  # Not supported in the "fish" shell.
  (cat ~/.cache/wal/sequences &)
fi

# User configuration

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
alias zshconfig="subl ~/.zshrc"
alias ohmyzsh="subl ~/.oh-my-zsh"
alias hostsconfig="subl /etc/hosts"
alias sshconfig="subl ~/.ssh/known_hosts"
alias alacrittyconfig="subl ~/.config/alacritty/alacritty.yml"
alias qtileconfig="subl ~/.config/qtile"

alias config='/usr/bin/git --git-dir=$HOME/.myconfig/ --work-tree=$HOME'

alias be="bundle exec"

alias portless-log="tail -f ~/.portless/daemon.log"

alias subseq="subl ~/logseq/logseq-main-graph"

alias walr="~/bin/manjaro/apply_wal.sh"

if [[ $(uname) == "Darwin" ]]; then
  fastfetch --logo macos
else
  fastfetch --logo manjaro
fi
