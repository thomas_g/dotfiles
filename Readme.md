# Dotfiles

This repository contains all my config files to speed up Macos and Manjaro Linux fresh install setups.

The way the dotfiles are versioned is describe in this tutorial: https://www.atlassian.com/git/tutorials/dotfiles

## Restore config files
The following script clones this repo and backup files that are already present on the system.
```
curl -Lks https://gitlab.com/thomas_g/dotfiles/raw/main/bin/install_config_versioning.sh | /bin/bash
```

## Gitconfig file

The `.gitconfig` file can be excluded from the local repository using:
```
# exclude
config update-index --skip-worktree .gitconfig
# include back
config update-index --no-skip-worktree .gitconfig
```

## OS specific guides

[Manjaro](docs/manjaro.md)

[Macos](docs/macos.md)
