#!/bin/bash

function git {
  local tmp=$(mktemp)
  local repo_name

  if [ "$1" = clone ] ; then
    command git "$@" --progress 2>&1 | tee $tmp
    repo_name=$(awk -F\' '/Cloning into/ {print $2}' $tmp)
    rm $tmp

    if [ -z "$repo_name" ]; then
      exit 1
    fi

    echo "Changing to directory $repo_name"
    cd $repo_name
    command $HOME/bin/git_scripts/prompt_config_user_name_and_email.sh
  else
    command git "$@"
  fi
}
