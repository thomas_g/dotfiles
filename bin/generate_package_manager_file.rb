#
# Usage: `ruby generate_package_manager_file.rb`
#
# You can specify optionnaly the path of the yml file and
# the path of the output file.
# Example: 
# ```
# PACKAGES_LIST_PATH=./my_file.yml \
# TARGET_FILE_PATH=./path_to/Brewfile \
# ruby generate_package_manager_file.rb
# ```
#

require 'yaml'
require 'fileutils'
require 'rubygems'

PACKAGES_LIST_PATH = ENV['PACKAGES_LIST_PATH'] || "#{Dir.home}/.config/package_managers/applications.yml"
PACKAGE_MANAGERS = {
  'Brew' => 'brew',
  'Pacman' => 'pacman'
}
TARGET_FILE_PATHS = {
  'brew' => "#{Dir.home}/.Brewfile",
  'pacman' => "#{Dir.home}/.config/pacman/pkglist.txt"
}

def check_dependencies
  missing_gems = %w[tty-prompt].reject { |gem| Gem::Specification.find_all_by_name(gem).any? }

  unless missing_gems.empty?
    puts "The following gems are missing: #{missing_gems.join(', ')}"
    puts "Installing them now..."
    system("sudo gem install #{missing_gems.join(' ')}") || begin
      puts "Failed to install gems. Please install them manually and try again."
      exit 1
    end
  end

  require 'tty-prompt'
end

def load_app_list(file_path)
  YAML.load_file(file_path)
rescue Errno::ENOENT
  puts "Configuration file not found: #{file_path}"
  exit 1
end

def select_package_manager(prompt)
  prompt.select("Choose a package manager:", PACKAGE_MANAGERS)
end

def select_apps(prompt, package_manager, apps_list)
  selected_apps = {}

  unless PACKAGE_MANAGERS.values.include? package_manager
    puts "The package manager #{package_manager} is not supported"
    exit 1
  end

  apps_list.each do |category, apps|
    defaults = []
    choices = apps.map do |name, app_attrs|
      next unless value = app_attrs[package_manager]

      defaults << name if app_attrs['selected']
      { name: name, value: value}
    end.compact

    next if choices.empty?

    selected = prompt.multi_select("Select applications for category: #{category}", choices, echo: false) do |menu|
      menu.default *defaults
    end

    next if selected.empty?

    selected_apps[category] = selected
  end
  selected_apps
end

def save_to_file(packages_list, target_file_path)
  FileUtils.mkdir_p(File.dirname(target_file_path))

  File.open(target_file_path, 'w') do |file|
    packages_list.each do |category, apps|
      file.puts "# #{category}"
      file.puts apps.join("\n")
      file.puts "\n"
    end
  end

  puts "Packages list saved to #{target_file_path}"
end

def target_file_path(package_manager)
  ENV['TARGET_FILE_PATH'] || TARGET_FILE_PATHS[package_manager]
end

def main
  check_dependencies
  prompt = TTY::Prompt.new

  apps_list = load_app_list(PACKAGES_LIST_PATH)
  package_manager = select_package_manager(prompt)

  packages_list = select_apps(prompt, package_manager, apps_list)

  if packages_list.empty?
    puts "No apps selected. Exiting."
    exit 0
  end

  target_file_path = target_file_path(package_manager)
  if prompt.yes?("Save changes to #{target_file_path}?")
    save_to_file(packages_list, target_file_path)
  else
    puts "No changes were saved."
  end
end

main
