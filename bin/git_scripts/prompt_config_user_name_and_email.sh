#!/bin/bash

echo "###"
echo "Setting up git user name and email"

if ! git tag > /dev/null 2>&1; then
  echo "This is not a git repository";
  exit 1
fi

# Set git user name
git_global_user_name=$(git config --global user.name)
echo "###"
echo "Enter git use name or press enter to skip (default is $git_global_user_name)"
read user_name
if [ -z "$user_name" ]
then
  echo "Skipping git user name, using global value ($git_global_user_name)"
else
  command git config user.name $user_name
  echo "Git user name set as $user_name"
fi

# Set git user email
git_global_user_email=$(git config --global user.email)
echo "###"
echo "Enter git use email or press enter to skip (default is $git_global_user_email)"
read user_email
if [ -z "$user_email" ]
then
  echo "Skipping git user email, using global value ($git_global_user_email)"
else
  command git config user.email $user_email
  echo "Git user email set as $user_email"
fi
