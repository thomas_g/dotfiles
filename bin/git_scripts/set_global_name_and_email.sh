#!/usr/bin/env bash

echo '=========='
echo "Do you want to set Git name and email? (Y/n):"
read response
response=$(echo "${response:-Y}" | tr '[:lower:]' '[:upper:]') # Default to 'Y' and normalize input

if [[ "$response" == "Y" ]]; then
  # Check existing global name and email
  current_name=$(git config --global user.name)
  current_email=$(git config --global user.email)

  if [[ -n "$current_name" ]]; then
    echo "Current Git global name: $current_name"
    echo "Do you want to override it? (Y/n):"
    read name_response
    name_response=$(echo "${name_response:-Y}" | tr '[:lower:]' '[:upper:]') # Default to 'Y'
  else
    name_response="Y"
  fi

  if [[ "$name_response" == "Y" ]]; then
    echo "Enter new Git global name:"
    read new_name
    git config --global user.name "$new_name"
    echo "Git global name set to: $new_name"
    echo ""
  fi

  if [[ -n "$current_email" ]]; then
    echo "Current Git global email: $current_email"
    echo "Do you want to override it? (Y/n):"
    read email_response
    email_response=$(echo "${email_response:-Y}" | tr '[:lower:]' '[:upper:]') # Default to 'Y'
  else
    email_response="Y"
  fi

  if [[ "$email_response" == "Y" ]]; then
    echo "Enter new Git global email:"
    read new_email
    git config --global user.email "$new_email"
    echo "Git global email set to: $new_email"
    echo ""
  fi
else
  # Display current Git global settings
  current_name=$(git config --global user.name)
  current_email=$(git config --global user.email)

  echo "Current Git global settings:"
  echo "Name: ${current_name:-<not set>}"
  echo "Email: ${current_email:-<not set>}"
fi

echo ''
