#!/bin/bash

function config {
   /usr/bin/git --git-dir=$HOME/.myconfig/ --work-tree=$HOME $@
}

cd $HOME
git clone --bare https://gitlab.com/thomas_g/dotfiles.git $HOME/.myconfig
config checkout

if [ $? = 0 ]; then
  echo "Checked out config.";
else
  echo "Backing up pre-existing dot files.";
  mkdir -p .config-backup
  config checkout 2>&1 | grep -E "\s+\." | awk {'print $1'} | xargs -I{} mv {} .config-backup/{}
  config reset --hard
fi;

config checkout
config config status.showUntrackedFiles no
echo "Dotfiles repository successfully cloned"
