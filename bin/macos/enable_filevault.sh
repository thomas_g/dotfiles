#!/usr/bin/env bash

echo '=========='
echo "Do you want to enable disk encryption? (Y/n):"
read response
response=$(echo "${response:-Y}" | tr '[:lower:]' '[:upper:]') # Default to 'Y' and normalize input

# Check FileVault status
encryption_status=$(fdesetup status)

if [[ "$response" == "Y" ]]; then
  if echo "$encryption_status" | grep -q "FileVault is Off"; then
    echo "Enabling FileVault..."
    # Use a temporary file for the recovery key output
    temp_file=$(mktemp)

    # Enable FileVault and write output to the temp file
    if sudo fdesetup enable -user "$USER" >"$temp_file" 2>&1; then
      # Move the temp file to the Desktop only if the command succeeds
      mv "$temp_file" "$HOME/Desktop/FileVault_recovery_key.txt"
      echo "FileVault has been enabled. The recovery key is saved on your Desktop."
    else
      # If enabling fails, show the error and clean up the temp file
      echo "Error: Failed to enable FileVault."
      echo "Details:"
      cat "$temp_file"
      rm -f "$temp_file"
    fi
  else
    echo "Current FileVault status:"
    echo "$encryption_status"
  fi
else
  echo "Current FileVault status:"
  echo "$encryption_status"
fi

echo ''
