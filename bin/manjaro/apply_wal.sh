#!/bin/bash

BACKGROUNDS_FOLDER="/usr/share/backgrounds"

# Check if the file exists and is a valid image
if [[ -n "$1" && "$1" =~ \.(jpg|jpeg|png)$ ]] && [ -f "$BACKGROUNDS_FOLDER/$1" ]; then
  file_name=$1
else
  file_name=''
fi

# Call Pywal
wal -i "$BACKGROUNDS_FOLDER/$file_name" --backend colorz

# Change Openrgb color
~/bin/manjaro/set_open_rgb_colors.sh

# Reload Qtile
qtile cmd-obj -o cmd -f reload_config
