#!/bin/bash

# List sources: `pactl list sinks short`
main="alsa_output.pci-0000_2f_00.4.analog-stereo"
headphones="alsa_output.usb-Logitech_PRO_X_Wireless_Gaming_Headset-00.analog-stereo"

if pactl list sinks short | grep 'RUNNING' | grep -q $main; then
  pactl set-default-sink $headphones
else
  pactl set-default-sink $main
fi
