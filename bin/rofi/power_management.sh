#!/bin/bash

declare -A texts
texts[lockscreen]="Lock screen"
texts[logout]="Log out"
texts[suspend]="Suspend"
texts[hibernate]="Hibernate"
texts[reboot]="Reboot"
texts[shutdown]="Shut down"

declare -A actions
actions[lockscreen]="xscreensaver-command -lock"
actions[logout]="qtile cmd-obj -o cmd -f shutdown"
actions[suspend]="systemctl suspend"
actions[hibernate]="systemctl hibernate"
actions[reboot]="systemctl reboot"
actions[shutdown]="systemctl poweroff"

ordered_entries=("lockscreen" "logout" "suspend" "hibernate" "reboot" "shutdown")

if [ -n "$1" ]; then
  for key in "${!texts[@]}"; do
    if [ "${texts[$key]}" == "$1" ]; then
      pkill rofi
      sleep 1

      ${actions[$key]}
    fi
  done
fi

for entry in "${ordered_entries[@]}"; do
  echo "${texts[$entry]}"
done
