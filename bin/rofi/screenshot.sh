#!/bin/bash

screenshots_dir="$HOME/screenshots"
mkdir -p "$screenshots_dir"

declare -A texts
texts[area]="Screenshot area"
texts[full_screen]="Screenshot full screens"

ordered_entries=("area" "full_screen")

if [ -n "$1" ]; then
  for key in "${!texts[@]}"; do
    if [ "${texts[$key]}" == "$1" ]; then
      file_path="$screenshots_dir/$(date +'%Y_%m_%d-%H_%M_%S').png"

      pkill rofi
      sleep 1

      if [ "$key" == "full_screen" ]; then
        import -window root "$file_path"
      else
        import "$file_path"
      fi
      exit 0
    fi
  done
fi

for entry in "${ordered_entries[@]}"; do
  echo "${texts[$entry]}"
done
