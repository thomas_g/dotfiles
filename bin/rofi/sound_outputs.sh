#!/bin/bash

declare -A texts
texts[analog_stereo]="Analog stereo"
texts[hdmi_stereo]="HDMI stereo"
texts[logitech_wireless]="Logitech PRO X"

declare -A devices
devices[analog_stereo]="alsa_output.pci-0000_2f_00.4.analog-stereo"
devices[hdmi_stereo]="alsa_output.pci-0000_2d_00.1.hdmi-stereo"
devices[logitech_wireless]="alsa_output.usb-Logitech_PRO_X_Wireless_Gaming_Headset-00.analog-stereo"

ordered_entries=("analog_stereo" "hdmi_stereo" "logitech_wireless")

if [ -n "$1" ]; then
  for key in "${!texts[@]}"; do
    if [ "${texts[$key]}" == "$1" ]; then
      pactl set-default-sink "${devices[$key]}"
      exit 0
    fi
  done
fi

for index in "${!ordered_entries[@]}"; do
  entry="${ordered_entries[$index]}"
  device="${devices[$entry]}"

  list=$(pactl list sinks short)

  # Check if this device is available
  if echo "$list" | grep -q "$device"; then
    # If the device is running, mark it as active in Rofi
    if echo "$list" | grep -q "$device.*RUNNING"; then
      echo -en "\0active\x1f$index\n"
    fi
    echo "${texts[$entry]}"
  fi
done
