#!/bin/bash

declare -A texts
texts[wallpaper]="Themes"
texts[sound]="Sound output"
texts[screenshot]="Take screenshot"
texts[power]="Power management"

declare -A modis
modis[wallpaper]="wallpapers"
modis[sound]="sound_outputs"
modis[screenshot]="screenshot"
modis[power]="power_management"

ordered_entries=("sound" "screenshot" "wallpaper" "power")

if [ -n "$1" ]; then
  for key in "${!texts[@]}"; do
    if [ "${texts[$key]}" == "$1" ]; then
      modi="${modis[$key]}"
      modi_path="$HOME/bin/rofi/$modi.sh"

      if [ -f "$modi_path" ]; then
        pkill rofi

        # Dynamically set the environment variable
        eval "export MODI_${modi^^}=true"
        PREVIEW=true rofi -show $modi -modi $modi:$modi_path
        exit 0
      else
        echo -en "\0message\x1fError: $modi_path does not exist\n"
      fi
    fi
  done
fi

for entry in "${ordered_entries[@]}"; do
  echo "${texts[$entry]}"
done
