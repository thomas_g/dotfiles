#!/bin/bash

BACKGROUNDS_FOLDER="/usr/share/backgrounds"

# If an argument is provided, set wallpaper with wal
if [ -n "$1" ]; then
  if [ "$1" == "Random" ]; then
    file_name=''
  else
    file_name=$1
  fi
  ~/bin/manjaro/apply_wal.sh >/dev/null 2>&1

  exit 0
else
  echo -en "Random\n"
  # List image files and pass them to Rofi with icons
  find $BACKGROUNDS_FOLDER -type f \( -iname "*.jpg" -o -iname "*.jpeg" -o -iname "*.png" \) | while read -r file; do
    # Output each file with an icon
    echo -en "$(basename "$file")\0icon\x1f$file\n"
  done
fi
