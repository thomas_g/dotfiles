# Kernel update

Manjaro Settings Manager offers a visual way to deal with kernels.

List the installed kernels:
```
mhwd-kernel -li
```

List all available kernels:
```
mhwd-kernel -l
```

Install a kernel:
```
sudo mhwd-kernel -i linux66
```
Then reboot the system.
