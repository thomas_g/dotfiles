# Services

System units need to be placed in /etc/systemd/system/ while user units can be placed in ~/.config/systemd/user.

Enable a service:
```
systemctl enable some-system-service.service
systemctl enable some-user-service.service --user
```

Manually start a service:
```
systemctl start some-system-service.service
systemctl start some-user-service.service --user
```

List unit files:
```
systemctl list-unit-files --state=enabled
systemctl list-unit-files --state=enabled --user
```

Check the logs of a unit:
```
sudo journalctl --unit some-system-service.service
sudo journalctl --user-unit some-user-service.service
```

Check the status of a services:
```
systemctl status some-system-service.service
systemctl status some-user-service.service --user
```

Reload the deamon after modifying a file:
```
systemctl daemon-reload
systemctl daemon-reload --user
```
