# Macos

## Prepare software installation

### Install Homebrew
Homebrew will also download and install Command Line Tools
https://brew.sh/
```
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```
Follow post install procedure displayed in terminal.

Then perfom a full system update:
```
softwareupdate --all --install --force
```


## Install softwares

### Install Oh My Zsh
https://ohmyz.sh/
```
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
```

### Install autosuggestions
```
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
```

### Clone git repo
```
curl -Lks https://gitlab.com/thomas_g/dotfiles/raw/main/bin/install_config_versioning.sh | /bin/bash

config update-index --skip-worktree .gitconfig
```

### Install Agnoster theme - download fonts
Powerline fonts: https://github.com/powerline/fonts
```
git clone https://github.com/powerline/fonts.git --depth=1
fonts/install.sh
rm -rf fonts

open "~/.config/solarized/Solarized Dark ansi.terminal"
```

In preferences > profiles > text, change font to `Meslo LG S for Powerline`.

### Install packages from Brewfile
Generate a Brewfile
```
ruby bin/generate_package_manager_file.rb
brew update
brew bundle --global
```


## Set preferences
Run `~/bin/macos/bootstrap_macos_settings.sh`


## Optional configuration

### Setup NAS mounts
Connect to the shared volumes you want to automount and save credentials in the keychain.
Then go to Settings > General > Login items > Navigate to the mounts and click add.

### Generate and/or copy ssh keys
```
ssh-keygen -t ed25519 -C "user@machine"
pbcopy $HOME/.ssh/id_ed25519.pub
```

- [ ] Setup Finder settings (Finder > Settings > Sidebar)
- [ ] Setup iStat Menus
- [ ] Setup web browsers
- [ ] Setup iCloud, Mail,calendar and contacts
