# Manjaro

## ![Desktop screenshot](https://gitlab.com/thomas_g/dotfiles/raw/main/screenshots/screenshot.png) ##

## Install softwares

### Install Yay
https://www.tecmint.com/install-yay-aur-helper-in-arch-linux-and-manjaro/
```
cd /opt
sudo git clone https://aur.archlinux.org/yay-git.git
sudo chown -R <user>:<group> yay-git
cd yay-git
sudo pacman -Syu
sudo pacman -S --needed base-devel git
makepkg -si
```

### Install Oh My Zsh
https://ohmyz.sh/#install
```
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
```

### Install autosuggestions
`git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions`

### Clone git repo
```
curl -Lks https://gitlab.com/thomas_g/dotfiles/raw/main/bin/install_config_versioning.sh | /bin/bash

config update-index --skip-worktree .gitconfig
```

### Install packages from pkglist
Install asdf and ruby
```
yay -S asdf-vm
asdf plugin add ruby
asdf install ruby latest
asdf global ruby x.x.x
```

Generate a pkglist
```
ruby bin/generate_package_manager_file.rb
```

This allows to comment the pkglist.txt file using #.
`grep -v "^#" ~/.config/pacman/pkglist.txt | yay -S --needed -`


## Download backgrounds
`sudo mkdir -p /usr/share/backgrounds`
- [Superultrawidewallpaper](https://superultrawidewallpaper.com/)
- [Wallhaven](https://wallhaven.cc/search?categories=111&purity=100&resolutions=5120x1440&sorting=relevance&order=desc&ai_art_filter=0&page=5)


## Install services

### Create symlinks for system services
```
sudo ln -s ~/.config/systemd/system/copy-wal-config.service /etc/systemd/system/copy-wal-config.service
sudo ln -s ~/.config/systemd/system/copy-wal-config.path /etc/systemd/system/copy-wal-config.path
```

### Enable services
```
systemctl enable copy-wal-config.service \
                 copy-wal-config.path
systemctl enable openrgb.service \
                 hyperion.service \
                 --user
```

### Optionally start services manually
```
systemctl start copy-wal-config.path
systemctl start openrgb.service \
                hyperion.service \
                --user
```

See [services documentation](linux/systemd.md)


## Set locales and keyboard preferences
```
sudo localectl set-locale LC_TIME=en_US.UTF-8
sudo localectl set-x11-keymap --no-convert us pc105 intl
```


## Edit gitconfig user and email
```
$HOME/bin/git_scripts/set_global_name_and_email.sh
```


## Setup NAS mounts
https://wiki.archlinux.org/index.php/samba#Automatic_mounting

Create a credentials file
```
sudo mkdir /etc/samba/credentials && cd "$_"
touch share
echo "username=xxx" > share
echo "password=xxx" > share
chown root:root /etc/samba/credentials
chmod 700 /etc/samba/credentials
chmod 600 /etc/samba/credentials/share
```

Create a folder for each volume
Exemple:
```
sudo mkdir /mnt/Myvolume
```

Edit `/etc/fstab`
Exemple:
```
//192.168.0.1/Myvolume /mnt/Myvolume cifs _netdev,vers=3.0,credentials=/etc/samba/credentials/share,file_mode=0666,dir_mode=0777 0 0
```


## Generate ssh keys
```
ssh-keygen -t ed25519 -C "user@machine"
cat $HOME/.ssh/id_ed25519.pub
```


## Resources

- [Kernel update](linux/kernel.md)
- [Nerdfonts icons directory](https://www.nerdfonts.com/cheat-sheet)
